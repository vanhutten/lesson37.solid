﻿using System;

namespace Lesson37.SOLID
{
    internal class GameController
    {
        private IGenerator _generator;
        private GameParameters _parameters;
        private IConsoleReader _reader;
        private IConsoleWriter _writer;

        private static GameController _instance;

        public bool IsInitialized { get; private set; }

        public static GameController GetInstatnce()
        {
            if (_instance == null)
            {
                _instance = new GameController();
            }
            return _instance;
        }

        public GameController()
        {
            IsInitialized = false; WinStatus = false;
        }

        public void InitController(IGenerator generator, GameParameters parameters, IConsoleReader reader, IConsoleWriter writer)
        {
            _generator = generator;
            _parameters = parameters;
            _reader = reader;
            _writer = writer;
            IsInitialized = true;
        }

        public void Start()
        {
            WinStatus = false;
            var RandomNumber = _generator.Generate(_parameters.Lower, _parameters.Upper);
            _writer.Write("Игра началась");
            for (int i = 0; i < _parameters.Rounds; i++)
            {
                _writer.Write($"Введите число. Осталось {_parameters.Rounds - i} попыток");
                var Guess = _reader.Read(false);
                var comparer = CompareNumbers(RandomNumber, Guess);
                Console.WriteLine($"{Answer(comparer)}");
                CheckWinCondition(RandomNumber, Guess);
                if (WinStatus) break;
            }
            GameOver();
        }

        public bool WinStatus { get; private set; }

        private int CompareNumbers(int number, int guess)
        {
            if (number == guess) return 0;
            if (number > guess) return -1;
            else return 1;
        }

        public void CheckWinCondition(int number, int guess)
        {
            if (CompareNumbers(number, guess) == 0)
            {
                WinStatus = true;
            }
        }

        private void GameOver()
        {
            _writer.Write($"Игра окончена вы {(WinStatus ? "победили" : "проиграли")}");
        }

        private string Answer(int CompareResult)
        {
            switch (CompareResult)
            {
                case 0:
                    return "Правильно!";

                case 1:
                    return "Меньше";

                case -1:
                    return "Больше";

                default:
                    return "Чушь!";
            }
        }

        public bool OnesMore()
        {
            _writer.Write("Сыграем еще? 1 - Да/ Любая другая цифра - Нет");
            if (_reader.Read(true) == 1)
            {
                return true;
            }
            return false;
        }
    }
}