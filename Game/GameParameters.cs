﻿namespace Lesson37.SOLID
{
    internal class GameParameters
    {
        public int Lower { get; private set; }
        public int Upper { get; private set; }
        public int Rounds { get; private set; }

        public GameParameters(IConsoleReader reader, IConsoleWriter writer)
        {
            while (true)
            {
                writer.Write("Задайте нижний предел диапазона");
                Lower = reader.Read(false);
                writer.Write("Задайте верхний предел диапазона");
                Upper = reader.Read(false);
                if (Lower > Upper)
                {
                    writer.Write("Нижний диапазон должен быть меньше верхнего");
                }
                else
                {
                    writer.Write("Задайте число попыток");
                    Rounds = reader.Read(true);
                    break;
                }
            }
        }
    }
}