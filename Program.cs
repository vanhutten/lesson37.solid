﻿using System;

namespace Lesson37.SOLID
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            IGenerator RandomGenerator = new Generator();

            var dataconsole = new DataConsole();
            var Controller = GameController.GetInstatnce();
            while (true)
            {
                var gameParameters = new GameParameters(dataconsole.GetReader(), dataconsole.GetWriter());
                Controller.InitController(RandomGenerator, gameParameters, dataconsole.GetReader(), dataconsole.GetWriter());
                if (Controller.IsInitialized)
                    Controller.Start();
                else
                    throw new TypeInitializationException("Controller", null);
                if (!Controller.OnesMore()) break;
            }
        }
    }
}