﻿namespace Lesson37.SOLID
{
    public interface IConsoleWriter
    {
        void Write(string text);

        void Write(int number);
    }
}