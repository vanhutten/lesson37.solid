﻿using System;

namespace Lesson37.SOLID
{
    internal class Generator : IGenerator
    {
        public int Generate(int lower, int upper)
        {
            if (lower < upper)
                return new Random().Next(lower, upper);
            else
                throw new ArgumentOutOfRangeException(message: "Lower bound is greater than upper", innerException: null);
        }
    }
}