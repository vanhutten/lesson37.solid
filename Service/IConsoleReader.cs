﻿namespace Lesson37.SOLID
{
    public interface IConsoleReader
    {
        int Read(bool IsPositive);
    }
}