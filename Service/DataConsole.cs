﻿using System;

namespace Lesson37.SOLID
{
    public class DataConsole : IConsoleReader, IConsoleWriter
    {
        public IConsoleReader GetReader()
        {
            return this;
        }

        public IConsoleWriter GetWriter()
        {
            return this;
        }

        public int Read(bool IsPositive)
        {
            while (true)
            {
                var line = Console.ReadLine();
                if (int.TryParse(line, out int number))
                    if (!IsPositive)
                        return number;
                    else
                    {
                        if (number > 0)
                            return number;
                        else
                            Write("Error number must be positive");
                    }
                else { Write("Error reading number"); }
            }
        }

        public void Write(string text) => Console.WriteLine(text);

        public void Write(int number) => Console.WriteLine(number);
    }
}