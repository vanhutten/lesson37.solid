﻿namespace Lesson37.SOLID
{
    internal interface IGenerator
    {
        int Generate(int lower, int upper);
    }
}